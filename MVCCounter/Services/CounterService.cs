﻿using MVCCounter.DataContext;
using MVCCounter.Models;
using System;
using System.Linq;

namespace MVCCounter.Services
{
    public class CounterService: ICounterService
    {
        private readonly ICounterContext _counterContext;

        public CounterService()
        {
            _counterContext = new CounterContext();
        }

        // used for unit test or depenency injection
        public CounterService(ICounterContext counterContext)
        {
            _counterContext = counterContext;
        }
        /// <summary>
        /// Increase number 
        /// </summary>
        /// <returns>The number after increasing</returns>
        public int IncreaseNumber()
        {
            try
            {
               
                    Counter counter = _counterContext.Counters.FirstOrDefault();

                    if (counter == null)
                    {
                        // add new record
                        counter = new Counter() { Number = 0 };
                    _counterContext.Counters.Add(counter);
                    }

                    if (counter.Number < 10)
                    {
                        counter.Number++;
                    _counterContext.SaveChanges();
                    }
                    return counter.Number;
               
            }
            catch (Exception ex)
            {
                // log exception
                throw ex;
            }
        }

        public int GetCurrentNumber()
        {

            Counter counter = _counterContext.Counters.FirstOrDefault() ?? new Counter() { Number = 0 };
            return counter.Number;
            
        }
    }
}