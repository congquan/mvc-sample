﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCCounter.Services
{
    public interface ICounterService
    {
        int IncreaseNumber();
        int GetCurrentNumber();
    }
}