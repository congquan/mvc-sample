﻿using MVCCounter.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MVCCounter.DataContext
{
    public interface ICounterContext : IDisposable
    {
        DbSet<Counter> Counters { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
        void MarkAsModified<T>(T item) where T : class;
    }
}