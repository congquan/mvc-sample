﻿using MVCCounter.Models;
using System.Data.Entity;

namespace MVCCounter.DataContext
{
    public class CounterContext : DbContext, ICounterContext
    {
        public CounterContext()
            :base("name=DefaultConnection")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<CounterContext>());
        }

        public DbSet<Counter> Counters { get; set; }


        public void MarkAsModified<T>(T item) where T : class
        {
            Entry(item).State = EntityState.Modified;

        }
    }
}