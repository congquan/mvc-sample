﻿using MVCCounter.DataContext;
using MVCCounter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCCounter.Controllers
{
    public class HomeController : Controller
    {

        private readonly ICounterService _counterService;
        public HomeController()
        {
            _counterService = new CounterService();
        }

        // used for unit test or depenency injection
        public HomeController(ICounterService counterService)
        {
            _counterService = counterService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var number = _counterService.GetCurrentNumber();
            return View(number);
        }
        [HttpPost]
        [ActionName("Index")]
        public ActionResult IndexPost()
        {
            var number = _counterService.IncreaseNumber();
            return View(number);
        }

    }    
}