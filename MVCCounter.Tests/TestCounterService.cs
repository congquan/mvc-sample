﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVCCounter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCCounter.Tests
{
    [TestClass]
    public class TestCounterService
    {
        [TestMethod]
        public void IncreaseNumber_FirstTime()
        {
            var context = new TestCounterContext();
            var service = new CounterService(context);
            var number = service.IncreaseNumber();

            Assert.AreEqual(number, 1);


        }

        [TestMethod]
        public void IncreaseNumber_Sequentially()
        {
            var context = new TestCounterContext();
            var service = new CounterService(context);
            var number = service.IncreaseNumber();
            Assert.AreEqual(number, 1);
            number = service.IncreaseNumber();
            Assert.AreEqual(number, 2);
            number = service.IncreaseNumber();
            Assert.AreEqual(number, 3);
            number = service.IncreaseNumber();
            Assert.AreEqual(number, 4);


        }

        [TestMethod]
        public void IncreaseNumber_ExceedTen()
        {
            var context = new TestCounterContext();
            var service = new CounterService(context);
            context.Counters.Add(new Models.Counter
            {
                Id = 1,
                Number = 10
            });
            var number = service.IncreaseNumber();
            Assert.AreEqual(number, 10);
            number = service.IncreaseNumber();
            Assert.AreEqual(number, 10);

        }
    }
}
