﻿using MVCCounter.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCCounter.Models;
using System.Data.Entity;

namespace MVCCounter.Tests
{
    public class TestCounterContext : ICounterContext
    {
        public DbSet<Counter> Counters { get; set; }

        public TestCounterContext()
        {
            this.Counters = new TestCouterDbSet();
        }


        public void Dispose()
        {
            
        }

        public void MarkAsModified<T>(T item) where T : class
        {
     
        }

        public int SaveChanges()
        {
            return 1;
        }

        public Task<int> SaveChangesAsync()
        {
            return Task.FromResult(1);
        }
    }
}
